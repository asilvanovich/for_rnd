#!/bin/bash
set -eu
TIMESTAMP="$(date +"%d-%M-%y_%H-%M-%S")"

docker build -t $DOCKER_REGISTRY:$CI_COMMIT_SHA-$TIMESTAMP .
docker push $DOCKER_REGISTRY:$CI_COMMIT_SHA-$TIMESTAMP

docker tag $DOCKER_REGISTRY:$CI_COMMIT_SHA-$TIMESTAMP $DOCKER_REGISTRY:latest
docker push $DOCKER_REGISTRY:latest

docker rmi $DOCKER_REGISTRY:$CI_COMMIT_SHA-$TIMESTAMP
docker system prune -f
