#!/bin/bash
if docker container ls | grep -q 'rnd_cont'; then
 sudo docker stop rnd_cont
 sudo docker rm rnd_cont
fi

docker run -p 8501:8501 --name rnd_cont -d $DOCKER_REGISTRY:latest
