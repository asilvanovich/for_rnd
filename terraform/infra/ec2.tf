data "aws_ami" "rnd_image" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn-ami-*-amazon-ecs-optimized"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

resource "aws_instance" "web" {
  ami                    = data.aws_ami.rnd_image.id
  instance_type          = var.instance_type
  subnet_id              = module.subnets.public_subnet_ids[0]
  vpc_security_group_ids = [aws_security_group.allow_inb.id]
  user_data              = templatefile("${path.module}/user_data.sh.tpl", { token = var.runner_token, url = var.gitlab_url, region = var.region, rep_url = aws_ecr_repository.rnd.repository_url })
  key_name               = "rnd"
  iam_instance_profile   = aws_iam_instance_profile.s3-mybucket-role-instanceprofile.name

  tags = {
    Name = "rnd"
  }
}

resource "aws_ecr_repository" "rnd" {
  name                 = "rnd_docker_repository"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }

 tags = {
    Name = "rnd"
  }
}

resource "aws_s3_bucket" "leverx_rnd" {
  bucket = "leverx-rnd"
  acl    = "private"

  tags = {
    Name        = "rnd"

  }
}


