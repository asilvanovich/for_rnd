variable "region" {
  type        = string
  description = "AWS Region"
}

variable "profile" {
  type        = string
  description = "AWS profile to use"
}

variable "namespace" {
  type        = string
  description = "Resource namespace"
}

variable "vpc_name" {
  type        = string
  description = "VPC name"
}

variable "cidr_block" {
  type        = string
  description = "A range of IP addresses to use in VPC"
}

variable "gitlab_url" {
  type        = string
  description = "Gitlab URL"
}

variable "runner_token" {
  type        = string
  description = "Gitlab runner token"
}


variable "instance_type" {
  type        = string
  description = "EC2 type"
}
