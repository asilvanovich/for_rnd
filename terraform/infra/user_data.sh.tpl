#!/bin/bash

#Install dependencies
sudo yum install wget python3 docker unzip zip -y

#Install aws CLI
export PATH="/usr/local/bin:$PATH"
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install

#Install gitlab-runner
echo 'installing gitlab-runner'
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
sudo yum install gitlab-runner -y


#Register gitlab-runner
echo 'registering gitlab-runner'
sudo gitlab-runner register --non-interactive --url ${url} \
--registration-token ${token} \
--executor "shell" \
--description "end" \
--tag-list "rnd" \
--run-untagged="true"
echo "gitlab-runner ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/gitlab-runner
usermod -aG docker gitlab-runner
echo 'setup complete!'

#login ECR 
su -c "aws ecr get-login-password --region ${region} | docker login --username AWS --password-stdin ${rep_url}" gitlab-runner