resource "aws_security_group" "allow_inb" {
  name        = "allow_ingress"
  description = "Allow inbound traffic"
  vpc_id      = module.vpc.vpc_id


  ingress {

    from_port   = 8501
    to_port     = 8501
    protocol    = "tcp"
    
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {

    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {

    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    
    cidr_blocks = ["0.0.0.0/0"]
  }

    egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }

  tags = {
    Name = "rnd"
  }
}
