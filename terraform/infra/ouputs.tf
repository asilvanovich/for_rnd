output "instance_ips" {
  value = aws_instance.web.public_ip
}

output "repository_url" {
  value = aws_ecr_repository.rnd.repository_url
}